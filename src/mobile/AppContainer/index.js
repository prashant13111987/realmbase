
import React, { Component } from 'react';
import ModuleContainer from '../ModuleContainers';
import { Provider } from 'react-redux';
import {StyleSheet , View} from "react-native";
import configureStore from '../../comman/store';

const store = configureStore();


class AppContainer extends Component {
    render() {

        return (
            <Provider store={store}>
                <ModuleContainer />
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    }
});

export default AppContainer;