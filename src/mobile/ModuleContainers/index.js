import React, { Component } from 'react';
import Realm from 'realm';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet,TouchableOpacity,TouchableHighlight} from 'react-native';
import { getObjects ,saveObjects ,modifyObjects,deleteObjects} from './../../comman/db'
import { Person } from './../../comman/schema';
import {connect} from "react-redux";
import { bindActionCreators } from 'redux';
import * as cafeListActionCreator from './../../comman/actions';
import {getCCDList} from "./../../comman/selector";


class App extends Component {
  constructor(props) {
    super(props);
    this.state = { realm: null };
  }

  componentDidMount() {
    // var key = new Int8Array(64); 
    // console.log("Encryption Key:::::",key)
    // Realm.open({
    //   schema: [{name: 'User', properties: {name: 'string'}}],encryptionKey: key
    // }).then(realm => {
    //   realm.write(() => {
    //     realm.create('User', {name: 'Prashant'});
    //   });
    //   this.setState({ realm });
    // });
    this.props.cafeListAction.getCafeList();
  //   const userObject = {
  //     email:'prashant.pawar@gep.com',
  //     userName:'P D Pawar',
  //     address:'Pune',
  //     userid:2
  //  }

  //  saveObjects(Person,'Person',userObject) .then((res) =>{
  //       console.log(res)
  //      getObjects(Person,'Person').then((result) =>{
  //       console.log("Fetch Successfully",result)
  //       let info = result.length > 0
  //       ? 'Number of Users in this Realm: ' + result.length
  //       : 'Loading...';
  //       this.setState({realm:info})

  //     //   const userObject = {
  //     //     email:'prashant.pawar@gep.com',
  //     //     userName:'A D Pawar',
  //     //     address:'Pune',
  //     //     userid:2
  //     //  }
  //     //  deleteObjects(Person,'Person',userObject).then((result) =>{
  //     //     console.log("Modified Successfully",result)
  //     //   })

  //     })
  //  })
  }

  componentWillUnmount() {
    // Close the realm if there is one open.
    // const {realm} = this.state;
    // if (realm !== null && !realm.isClosed) {
    //   realm.close();
    // }
  }

  save() {
    const userObject = {
      email:'prashant.pawar@gep.com',
      userName:'P D Pawar',
      address:'Pune',
      userid:2
   }

   saveObjects(Person,'Person',userObject) .then((res) =>{
        console.log(res)
       getObjects(Person,'Person').then((result) =>{
        console.log("Fetch Successfully",result)
        let info = result.length > 0
        ? 'Number of Users in this Realm: ' + result.length
        : 'Loading...';
        this.setState({realm:info})

      //   const userObject = {
      //     email:'prashant.pawar@gep.com',
      //     userName:'A D Pawar',
      //     address:'Pune',
      //     userid:2
      //  }
      //  deleteObjects(Person,'Person',userObject).then((result) =>{
      //     console.log("Modified Successfully",result)
      //   })

      })
   })
  }

  render() {
    // const info = this.state.realm
    //   ? 'Number of Users in this Realm: ' + this.state.realm.objects('User').length
    //   : 'Loading...';

    console.log("Cafe List Data inside Container :::::",this.props.cafeListData)
    let info  = this.state.realm
    

    return (
      <View style={{flex:1,justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
        <Text>
          {info}
          {/* <TouchableHighlight
              onPress={() => this.save()}
              style={{flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 20}}
            >
              <View style={{backgroundColor:'gray'}}>
                <Text style={{ color: 'white',
    fontSize: 14,
    paddingHorizontal: 16,
    paddingVertical: 8}}>Save</Text>
              </View>
            </TouchableHighlight> */}
        </Text>
      </View>
    );
  }
}

App.propTypes = {
    cafeListData: PropTypes.array,
    navigation: PropTypes.object,
    cafeListAction: PropTypes.shape({
        getCafeList: PropTypes.func,
    }),
};

const mapStateToProps = state => ({
    cafeListData: getCCDList(state),
    loading:state.cafeListReducer.loading,
});

const mapDispatchToProps = dispatch => ({
    cafeListAction: bindActionCreators(cafeListActionCreator, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
