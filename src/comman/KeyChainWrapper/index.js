  
import React, { Component } from 'react';
import * as Keychain from 'react-native-keychain';

export const save = (username,password) => new Promise((resolve, reject) => {
  try {
    Keychain.setGenericPassword(username, password).then(()=>{
      resolve("Save Succesfully")
    })
  } catch (error) {
    reject("Failed to Save")
  }
});

export const saveAuth = (token) => new Promise((resolve, reject) => {
  try {
    Keychain.setGenericPassword(token).then(()=>{
      resolve("Save Succesfully")
    })
  } catch (error) {
    reject("Failed to Save")
  }
});

export const getAuth = () => new Promise((resolve, reject) => {
  try {
      Keychain.getGenericPassword().then((token) =>{
          if (token) {
               resolve(token)
             }
      })
    } catch (err) {
       reject("Could not save token")
    }
});

export const load = () => new Promise((resolve, reject) => {
    try {
        Keychain.getGenericPassword().then((...credentials) =>{
            if (credentials) {
                console.log("Credentials loaded!",...credentials)
                 resolve(...credentials)
               }
        })
      } catch (err) {
         reject("Could not save credentials")
      }
});

export const reset = () => new Promise((resolve, reject) => {
  try {
      Keychain.resetGenericPassword().then(() =>{
        resolve("Reset Credentials Success")
      })
    } catch (err) {
       reject("Reset Credentials Error")
    }
});

