import Realm from 'realm';
import { Person } from '../schema';
const key = new Int8Array(64); 
//encryptionKey: key

export const getObjects = (schema,schemaValue) => new Promise((resolve, reject) => {
    Realm.open({ schema: [schema]})
        .then((realm) => {
            if (realm.objects(schemaValue)) {
                if (realm.objects(schemaValue).length > 0) {
                    resolve(realm.objects(schemaValue));
                } else {
                    resolve(null);
                }
            } else {
                resolve(null);
            }
        })
        .catch((error) => {
            reject(error);
        });
});

export const saveObjects = (schema,schemaValue,data) => new Promise((resolve, reject) => {   
    Realm.open({ schema: [schema]})
        .then((realm) => {
            realm.write(() => {
               // const tempObject = Object.assign({}, userObject, { email: userObject.email });
                realm.create(schemaValue, data, true);
                resolve("Save Successfully !!!!!");
            });
        })
        .catch((error) => {
            console.log("Error ??????", error.toString())
            reject(error);
        });
});

export const modifyObjects = (schema,schemaValue,data) => new Promise((resolve, reject) => {   
    Realm.open({ schema: [schema]})
        .then((realm) => {
            realm.write(() => {
               // const tempObject = Object.assign({}, userObject, { email: userObject.email });
                realm.create(schemaValue, data, 'modified');
                resolve("Modified Successfully !!!!!");
            });
        })
        .catch((error) => {
            console.log("Error ??????", error.toString())
            reject(error);
        });
});

export const deleteObjects = (schema,schemaValue,data) => new Promise((resolve, reject) => {
    Realm.open({ schema: [schema]})
        .then((realm) => {
            realm.write(() => {
                const objectDel = realm.create(schemaValue, data, true);
                realm.delete(objectDel);
                resolve("Successfully Deleted");
            });
        })
        .catch((error) => {
            reject(error);
        });
});

export const deleteAllObjects = (schema,schemaValue) => new Promise((resolve, reject) => {
    Realm.open({ schema: [schema]})
        .then((realm) => {
            realm.write(() => {
                const tables = realm.objects(schemaValue);
                realm.delete(tables);
                resolve();
            });
        })
        .catch((error) => {
            reject(error);
        });
});

export const closeRealm = () =>  {
    if (Realm !== null && !Realm.isClosed) {
        Realm.close();
      }
};
