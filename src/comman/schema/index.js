
export const Person = {
    name: 'Person',
    primaryKey: 'userid',
    properties: {
        email:'string',
        userName:'string',
        address:'string',
        userid:'int',
    },
};

export const cafeObject = {
    name: 'Cafe',
    primaryKey: 'id',
    properties: {
        title:'string',
        ingredients:'string',
        thumbnail:'string',
        id:'int',
        isSelected:'bool',
    },
};

// title: 'Onion and Fresh Herb Omelet with Mixed Greens',
//     href: 'http://find.myrecipes.com/recipes/recipefinder.dyn?action=displayRecipe&recipe_id=1622444',
//     ingredients: 'egg substitute, milk, parsley, thyme, salt, black pepper, eggs, flour, nonstick cooking spray, onions, garlic, salad greens, salad greens, red wine vinegar, olive oil, goat cheese, almonds',
//     thumbnail: 'http://img.recipepuppy.com/514820.jpg',
//     isSelected: false,
//     id: 9 