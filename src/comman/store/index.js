// import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import { applyMiddleware, createStore } from 'redux';
import rootReducer from './../AppReducers';
import rootSaga from './../sagas/index';

export default function configureStore() {

    const sagaMiddleware = createSagaMiddleware();
    // const middleware = createReactNavigationReduxMiddleware(
    //     'root',
    // );
    const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
    sagaMiddleware.run(rootSaga);
    return store;
}
