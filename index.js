/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/mobile/AppContainer';
// import Keychain from './KeyChain';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
